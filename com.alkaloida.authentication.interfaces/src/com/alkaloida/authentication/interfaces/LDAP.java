/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alkaloida.authentication.interfaces;

import javax.ws.rs.PathParam;

/**
 *
 * @author szlelesz
 */
public interface LDAP {
   public User login(@PathParam("name") String username);
}
