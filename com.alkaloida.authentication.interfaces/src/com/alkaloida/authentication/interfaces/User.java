/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alkaloida.authentication.interfaces;

/**
 *
 * @author szlelesz
 */
public interface User {

    public User getHead();

    public String getAccount();

    public String getCode();

    public String getFirstName();

    public String getLastName();

    public String getJobTitle();
}
