/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alkaloida.xml;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author szlelesz
 */
public interface XmlParser {
    public XmlContentHandler getContentHandler();
    public void setStream(InputStream stream);
    public void parse() throws SAXException, IOException, ParserConfigurationException;
    
}
