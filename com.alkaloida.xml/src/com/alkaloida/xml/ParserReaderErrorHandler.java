/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alkaloida.xml;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 *
 * @author szlelesz
 */
final class ParserReaderErrorHandler implements ErrorHandler{

    @Override
    public void warning(SAXParseException exception) throws SAXException {
        throw new ParserWarningRuntimeException(exception);
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        throw new ParserErrorRuntimeException(exception);
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        throw new ParserFatalErrorRuntimeException(exception);
    }
    
}
