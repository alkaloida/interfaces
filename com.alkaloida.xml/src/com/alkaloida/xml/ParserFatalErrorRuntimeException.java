/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alkaloida.xml;

/**
 *
 * @author szlelesz
 */
public class ParserFatalErrorRuntimeException extends ParserErrorRuntimeException{
    private static final long serialVersionUID = 1L;
    public ParserFatalErrorRuntimeException(Throwable cause) {
        super(cause);
    }
    
    
}
