/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alkaloida.xml;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 *
 * @author szlelesz
 */
public abstract class XmlContentHandler extends DefaultHandler{
    private final SAXParserFactory factory;
    private final SchemaFactory schemaFactory;
    private ErrorHandler errorHandler;
    
    private SAXParser parser;
    private XMLReader reader;
    public XmlContentHandler(){
        super();
        this.factory = SAXParserFactory.newInstance();        
        this.factory.setNamespaceAware(true);
        this.schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");                
    }
    
    public final void setXmlSxhemaDefinition(URL url) throws SAXException{
        Schema compiledSchema = schemaFactory.newSchema(url);
        this.factory.setSchema(compiledSchema);
    }
    
    public final void setErrorhandler(ErrorHandler errorHandler){
        this.errorHandler = errorHandler;
    }
    
    @Override
    public abstract void startDocument() throws SAXException;

    @Override
    public abstract void endDocument() throws SAXException;
    
    @Override
    public abstract void characters(char[] ch, int start, int length) throws SAXException;
                

    @Override
    public abstract void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException;

    @Override
    public abstract void endElement(String uri, String localName, String qName) throws SAXException;    
        
    public final void load(InputStream xmlStream) throws SAXException, IOException, ParserConfigurationException{        
            this.parser= factory.newSAXParser();
            this.reader = parser.getXMLReader();     
            this.reader.setContentHandler(this);
            this.reader.setErrorHandler((this.errorHandler != null)?this.errorHandler:new ParserReaderErrorHandler());        
            this.reader.parse(new InputSource(xmlStream));
    }        
}
